#!/bin/bash
set -euo pipefail

# create database and user for ghost
echo "create database ${GHOST_MYSQL_DATABASE};" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
echo "create user '${GHOST_MYSQL_USERNAME}'@'%' identified by '${GHOST_MYSQL_PASSWORD}';" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
echo "grant all privileges on ${GHOST_MYSQL_DATABASE}.* to '${GHOST_MYSQL_USERNAME}'@'%';" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
