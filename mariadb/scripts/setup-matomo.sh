#!/bin/bash
set -euo pipefail

# create database and user for matomo
echo "create database ${MATOMO_MYSQL_DATABASE};" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
echo "create user '${MATOMO_MYSQL_USERNAME}'@'%' identified by '${MATOMO_MYSQL_PASSWORD}';" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
echo "grant all privileges on ${MATOMO_MYSQL_DATABASE}.* to '${MATOMO_MYSQL_USERNAME}'@'%';" | mysql -u root --password=${MYSQL_ROOT_PASSWORD}
