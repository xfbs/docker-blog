# Blog

This is the code that powers my blog -- <https://xfbs.net/>. It consists of a
couple of docker containers orchestrated by docker-compose. Included is:

- MySQL Database
- Matomo Analytics
- Backups to Backblaze
- NGINX with Let's Encrypt
- Ghost Blogging Platform

All of these are pre-configured and set up the way they should, for the most
part. 

The goal is to have everything configured such that backups are performed
automatically, and at any point everything can be wiped, and the state will
be restored from the backups.

## Setup

You need to have docker and docker-compose installed.

    # apt install docker docker-compose

All of the configuration exists inside the `example.env` file. You have to
change the default passwords in order to create a secure installation. You
also need a backblaze bucket with credentials to save backups to, and a domain
name pointing to the server that this runs on in order to create valid TLS
certificates.

You should make a copy of the provided `example.env` file, naming it something
appropriate (such as `production.env`). You have to populate it with some
passwords, which can be chosen at random, for these items:

- `MYSQL_ROOT_PASSWORD`
- `MATOMO_MYSQL_PASSWORD`
- `GHOST_MYSQL_PASSWORD`

Next, appropriate settings have to be put in for some other items, such as:

- Set `GHOST_URL` to the external URL, such as `https://xfbs.net/`.

## Running

Everything can be launched using `docker-compose`. You should pass the name of
the settings file that you want to use to it with the `--env-file` flag, for
example to launch it with the sample configuration run it like this.

    docker-compose --env-file example.env up

The first time you launch everything, you have to set up everything. To do so,
visit both the Ghost admin panel and the matomo panel.

## License

MIT.
